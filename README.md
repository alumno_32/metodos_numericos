import numpy as np
from numpy import exp, cos, pi, sin, arcsin
from matplotlib import pyplot as plt
from sympy import Symbol, Eq, solve
from sympy.solvers.solvers import nsolve
from scipy.optimize import fsolve

def ds(s,i):
    return -b1*s*i   
def dr(i):
    return i*1/3
def di(s,i):
    return (b1*s*i)-k*i

a=0
b=140
n=150
h = (b-a)/n 
s0 = 1
i0 = 1.27*10**(-6)
r0 = 0
b1= 1/2
k=1/3
u_n = Symbol('u_n')

si_l = [s0]
ii_l = [i0]
ri_l = [r0]
si_l1 = [s0]
ii_l1 = [i0]
ri_l1 = [r0]
si_l2 = [s0]
ii_l2 = [i0]
ri_l2 = [r0]
si_l3 = [s0]
ii_l3 = [i0]
ri_l3 = [r0]
si_l4 = [s0]
ii_l4 = [i0]
ri_l4 = [r0]
si_l5 = [s0]
ii_l5 = [i0]
ri_l5 = [r0]
si_l6 = [s0]
ii_l6 = [i0]
ri_l6 = [r0]
#euler_explicito   
for i in range(n): 
    #euler_explicito
    si_l.append(si_l[-1] +h*ds(si_l[-1],ii_l[-1]))
    ii_l.append(ii_l[-1] +h*di(si_l[-2],ii_l[-1]))
    ri_l.append(ri_l[-1] +h*dr(ii_l[-2]))
    #Punto_Medio
    si_l2.append(si_l2[-1] +h*ds(si_l2[-1]+(ds(si_l2[-1],ii_l2[-1])*h/2),ii_l2[-1]))
    ii_l2.append(ii_l2[-1] +h*di(si_l2[-2],(ii_l2[-1]+(di(si_l2[-2],ii_l2[-1])*h/2))))
    ri_l2.append(ri_l2[-1] +h*dr(ii_l2[-1]+((h/2)*dr(ii_l2[-2]))))
    #euler_Modificado
    si_l3.append(si_l3[-1] +(h/2)*(ds(si_l3[-1],ii_l3[-1])+h*ds(si_l3[-1]+(ds(si_l3[-1],ii_l3[-1])),ii_l3[-1])))
    ii_l3.append(ii_l3[-1] +(h/2)*(di(si_l3[-2],ii_l3[-1])+h*di(si_l3[-1],ii_l3[-1]+(di(si_l3[-2],ii_l3[-1])))))
    ri_l3.append(ri_l3[-1] +(h/2)*(dr(ii_l3[-2])+h*dr(ii_l3[-2]+(dr(ii_l3[-2])))))
    #Heun
    si_l4.append(si_l4[-1] +(h/4)*(ds(si_l4[-1],ii_l4[-1])+3*ds(si_l4[-1]+(2/3)*h*ds(si_l4[-1],ii_l4[-1]),ii_l4[-1])))
    ii_l4.append(ii_l4[-1] +(h/4)*(di(si_l4[-2],ii_l4[-1])+3*di(si_l4[-2],ii_l4[-1]+(2/3)*h*di(si_l4[-2],ii_l4[-1]))))
    ri_l4.append(ri_l4[-1] +(h/4)*(dr(ii_l4[-2])+3*dr(ii_l4[-2]+(2/3)*h*dr(ii_l4[-2]))))
    #Rk_de_4pasos
    ks1=ds(si_l5[-1],ii_l5[-1])
    ks2=ds((si_l5[-1]+(h/2)*ks1),ii_l5[-1])
    ks3=ds((si_l5[-1]+(h/2)*ks2),ii_l5[-1])
    ks4=ds((si_l5[-1]+h*ks3),ii_l5[-1])
    si_l5.append(si_l5[-1] +(h/6)*(ks1+2*ks2+2*ks3+ks4))
    ki1=di(si_l5[-2],ii_l5[-1])
    ki2=di(si_l5[-2],(ii_l5[-1]+(h/2)*ki1))
    ki3=di(si_l5[-2],(ii_l5[-1]+(h/2)*ki2))
    ki4=di(si_l5[-2],(ii_l5[-1]+h*ki3))
    ii_l5.append(ii_l5[-1] +(h/6)*(ki1+2*ki2+2*ki3+ki4))
    kr1=dr(ii_l5[-2])
    kr2=dr(ii_l5[-2]+(h/2)*kr1)
    kr3=dr(ii_l5[-2]+(h/2)*kr2)
    kr4=dr(ii_l5[-2]+h*kr3)
    ri_l5.append(ri_l5[-1] +(h/6)*(kr1+2*kr2+2*kr3+kr4))
    #euler_implicito
    si_l1.append(nsolve(Eq(si_l1[-1] +h*ds(u_n,ii_l1[i]),u_n),u_n,1))
    ii_l1.append(nsolve(Eq(ii_l1[-1] +h*di(si_l1[-2],u_n),u_n),u_n,0))
    ri_l1.append(nsolve(Eq(ri_l1[-1] +h*dr(ii_l1[-2]),u_n),u_n,0))
    #trapecio
    si_l6.append(nsolve(Eq(si_l6[-1] +(h/2)*(ds(u_n,ii_l6[i])+ds(si_l6[-1],ii_l6[i])),u_n),u_n,1))
    ii_l6.append(nsolve(Eq(ii_l6[-1] +(h/2)*(di(si_l6[-2],u_n)+di(si_l6[-2],ii_l6[i])),u_n),u_n,0))
    ri_l6.append(nsolve(Eq(ri_l6[-1] +(h/2)*(dr(ii_l6[-2])+dr(ii_l6[-2])),u_n),u_n,0))
    
xs = np.linspace(a,b,n+1) 

plt.plot(xs,si_l, label="S(t)")
plt.plot(xs,ii_l,label="I(t)")
plt.plot(xs,ri_l, label="R(t)")
plt.title('Método  Euler explicito')
plt.xlabel('x')
plt.ylabel('y')
plt.legend()
plt.grid()
plt.show()
plt.plot(xs,si_l2, label="S(t)")
plt.plot(xs,ii_l2,label="I(t)")
plt.plot(xs,ri_l2, label="R(t)")
plt.title('Método  punto medio')
plt.xlabel('x')
plt.ylabel('y')
plt.legend()
plt.grid()
plt.show()
plt.plot(xs,si_l3, label="S(t)")
plt.plot(xs,ii_l3,label="I(t)")
plt.plot(xs,ri_l3, label="R(t)")
plt.title('Método  Euler Modificado')
plt.xlabel('x')
plt.ylabel('y')
plt.legend()
plt.grid()
plt.show()
plt.plot(xs,si_l4, label="S(t)")
plt.plot(xs,ii_l4,label="I(t)")
plt.plot(xs,ri_l4, label="R(t)")
plt.title('Método  heun')
plt.xlabel('x')
plt.ylabel('y')
plt.legend()
plt.grid()
plt.show()
plt.plot(xs,si_l5, label="S(t)")
plt.plot(xs,ii_l5,label="I(t)")
plt.plot(xs,ri_l5, label="R(t)")
plt.title('Método  RK explicito de 4 etapas')
plt.xlabel('x')
plt.ylabel('y')
plt.legend()
plt.grid()
plt.show()
plt.plot(xs,si_l1, label="S(t)")
plt.plot(xs,ii_l1,label="It)")
plt.plot(xs,ri_l1, label="R(t)")
plt.title('Método  Euler implicito')
plt.xlabel('x')
plt.ylabel('y')
plt.legend()
plt.grid()
plt.show()
plt.plot(xs,si_l6, label="S(t)")
plt.plot(xs,ii_l6,label="I(t)")
plt.plot(xs,ri_l6, label="R(t)")
plt.title('Método del trapecio')
plt.xlabel('x')
plt.ylabel('y')
plt.legend()
plt.grid()
plt.show()